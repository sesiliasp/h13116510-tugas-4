import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class PermutasidanKombinasi {
	
	static void Menu(){
		System.out.println("1. Permutasi");
		System.out.println("2. Kombinasi");
		System.out.println("0. Keluar");
	}
	
	static void Permutasi(){
		System.out.println("Program untuk mencari  nilai Permutasi");
		try{
			File NilaiN = new File("NilaiN.txt");
			FileReader ReaderN = new FileReader(NilaiN);
			BufferedReader bufferedReader = new BufferedReader(ReaderN);
			StringBuffer BufferN = new StringBuffer();
			String lineN = null;
			
			File NilaiR = new File("NilaiR.txt");
			FileReader ReaderR = new FileReader(NilaiR);
			BufferedReader bufferedReaderR = new BufferedReader(ReaderR);
			StringBuffer BufferR = new StringBuffer();
			String lineR = null;
			
			while ((lineN = bufferedReader.readLine()) != null && (lineR = bufferedReaderR.readLine()) != null){
				int LineN = Integer.parseInt(lineN);
				int LineR = Integer.parseInt(lineR);
				
				if(LineN>0 && LineR>0 && LineN>LineR){
					int Number = LineN - LineR;
					int Nilai1=1, Nilai2=1, Nilai3 =1;
					
					for(int indexI=1; indexI<=LineN; indexI++){
						Nilai1 = Nilai1*indexI;
					}
					for(int indexI=1; indexI<=LineR; indexI++){
						Nilai2 = Nilai2*indexI;
					}
					for(int indexI=1; indexI<=Number; indexI++){
						Nilai3 = Nilai3*indexI;
					}
					
					int RumusPermutasi = Nilai1/Nilai3;
					System.out.println("Hasil Permutasi : "+RumusPermutasi);
				}
				else{
					System.out.println("Silahkan input kembali");
				}
				
				System.out.println("Nilai n : "+LineN);
			
				System.out.println("Nilai r: "+LineR);
				
			}
			ReaderN.close();
			ReaderR.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}
	
	static void Kombinasi(){
		try{
			File NilaiN = new File("NilaiN.txt");
			FileReader ReaderN = new FileReader(NilaiN);
			BufferedReader bufferedReader = new BufferedReader(ReaderN);
			StringBuffer BufferN = new StringBuffer();
			String lineN = null;
			
			File NilaiR = new File("NilaiR.txt");
			FileReader ReaderR = new FileReader(NilaiR);
			BufferedReader bufferedReaderR = new BufferedReader(ReaderR);
			StringBuffer BufferR = new StringBuffer();
			String lineR = null;
			
			while ((lineN = bufferedReader.readLine()) != null && (lineR = bufferedReaderR.readLine()) != null){
				int LineN = Integer.parseInt(lineN);
				int LineR = Integer.parseInt(lineR);
				
				if(LineN>0 && LineR>0 && LineN>LineR){
					int Number = LineN - LineR;
					int Nilai1=1, Nilai2=1, Nilai3 =1;
					
					for(int indexI=1; indexI<=LineN; indexI++){
						Nilai1 = Nilai1*indexI;
					}
					for(int indexI=1; indexI<=LineR; indexI++){
						Nilai2 = Nilai2*indexI;
					}
					for(int indexI=1; indexI<=Number; indexI++){
						Nilai3 = Nilai3*indexI;
					}
					
					int RumusKombinasi = Nilai1/(Nilai3*Nilai2);
					System.out.println("Hasil Kombinasi : "+RumusKombinasi );
				}
				else{
					System.out.println("Silahkan input kembali");
				}
				
				System.out.println("Nilai n : "+LineN);
			
				System.out.println("Nilai r : "+LineR);
				
			}
			ReaderN.close();
			ReaderR.close();
		}
		catch(Exception e){
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {
		BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String inputData = null;
        int choice = 0;
        do {
            Menu();
            System.out.println("Masukkan Pilihan Anda: ");
            try {
                inputData = bufferedReader.readLine();
                try  {
                    choice = Integer.parseInt(inputData);
                    if (choice > 0 && choice == 1) {
                        Permutasi();
                    }
                    else if (choice > 0 && choice == 2) {
                        Kombinasi();
                    }
                    else {
                        System.out.println("Thankyou ^_^");
                    }

                }
                catch(NumberFormatException e) {
                    System.out.println("Masukan Tidak Sesuai");
                }
            }
            catch (IOException error) {
                System.out.println("Error Input " + error.getMessage());
            }

        } while(choice > 0);
		
	}

}
