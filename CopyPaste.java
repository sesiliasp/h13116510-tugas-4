import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class CopyPaste {

	public static void main(String[] args) {
		InputStream inStream = null;
		OutputStream outStream = null;
		
		try {
			File filepertama = new File("Folderpertama.txt");
			File filekedua = new File("Folderkedua.txt");
			
			inStream = new FileInputStream(filepertama);
			outStream = new FileOutputStream(filekedua);
			
			byte[] buffer= new byte [1024];
			
			int length;
			while ((length = inStream.read(buffer))> 0){
				
				outStream.write(buffer, 0, length);
			}
			
			inStream.close();
			outStream.close();
			System.out.println("Proses copy File Berhasil");
			
		}catch(IOException e){
			e.printStackTrace();
		}

	}

}