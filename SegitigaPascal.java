import java.util.Scanner;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.FileWriter;

public class SegitigaPascal {

	public static void main(String[] args) throws IOException {
		int spasi, i, j;
		Scanner s = new Scanner(System.in);
		
		try (FileWriter f0 = new FileWriter("HasilSegitigaPascal.txt")){
			String newLine = System.getProperty("line.separator");
			System.out.print("Nilai tinggi segitiga : ");
			spasi = s.nextInt();
			int[][]b = new int[100][100];
		    for(i=1; i<=spasi; i=i+1){
		        for(j=spasi; j>i; j=j-1){
		            f0.write("   ");
		        }
		            
		        for(j=1; j<=i; j++){
		            if(i==0||j==i){
		                b[i][j]=1;
		                f0.write("  "+b[i][j]+"   ");
		            }else{
		                b[i][j] = b[i-1][j-1] + b[i-1][j];
		                
		                if(b[i][j]>99){
		                	f0.write("  "+b[i][j]+"   ");
		            	}else if(b[i][j]>9){
		                	f0.write("  "+b[i][j]+"   ");
		                }else{
		                	f0.write("  "+b[i][j]+"   ");
		                }
		            }
		        }
		            f0.write(newLine);	
			}
		}
	}
}