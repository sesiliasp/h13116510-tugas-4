import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.FileNotFoundException;

public class Fungsi {

	public static void main(String[] args) {

		try {
  			File filePertama = new File("NilaiA.txt");
  			FileReader fileReaderPertama = new FileReader(filePertama);
  			BufferedReader bufferedReaderPertama = new BufferedReader(fileReaderPertama);
  			StringBuffer stringBufferPertama = new StringBuffer();
  			String linePertama = null;

          		File fileKedua = new File("NilaiB.txt");
          		FileReader fileReaderKedua = new FileReader(fileKedua);
          		BufferedReader bufferedReaderKedua = new BufferedReader(fileReaderKedua);
          		StringBuffer stringBufferKedua = new StringBuffer();
          		String lineKedua = null;

          		File fileKetiga = new File("NilaiC.txt");
          		FileReader fileReaderKetiga = new FileReader(fileKetiga);
          		BufferedReader bufferedReaderKetiga = new BufferedReader(fileReaderKetiga);
          		StringBuffer stringBufferKetiga = new StringBuffer();
          		String lineKetiga = null;
  			
        while ((linePertama = bufferedReaderPertama.readLine()) != null && (lineKedua = bufferedReaderKedua.readLine()) != null && (lineKetiga = bufferedReaderKetiga.readLine()) != null) {
          
          	int nilai1 = Integer.parseInt(linePertama);
          	int nilai2 = Integer.parseInt(lineKedua);
          	int nilai3 = Integer.parseInt(lineKetiga);

          System.out.println("Letak Fungsi Grafik");

          	System.out.println("Nilai A = "+nilai1);
          	System.out.println("Nilai B = "+nilai2);
          	System.out.println("Nilai C = "+nilai3);

          System.out.println("= "+nilai1+"x^2"+"+"+nilai2+"x"+"+"+nilai3);

          int TitikBalikX = -(nilai2/2*nilai1);

          int Nilai4 = ((int)Math.pow(nilai2, 2) - (4*nilai1*nilai3));
          int TitikBalikY = -(Nilai4/4*nilai1);

          System.out.println("Titik baliknya = ("+TitikBalikX+","+TitikBalikY+")");

          if(nilai1>0){
            System.out.println("Grafik terbuka keatas, titik balik minimum");

            if(nilai3 > 0){
              System.out.println("Grafik memotong sumbu Y, diatas sumbu X");

              if(nilai1*nilai2 > 0){
                System.out.println("Titik balik terletak dikiri sumbu Y");
              }else if(nilai1*nilai2 < 0){
                System.out.println("Titik balik terletak dikiri sumbu Y");
              }else{
                System.out.println("Titik balik terletak di sumbu Y");
              }

            }else if(nilai3 < 0){
              System.out.println("Grafik memotong sumbu Y, dibawah sumbu X");

              if(nilai3 > 0){
                System.out.println("Grafik memotong sumbu Y, diatas sumbu X");
              }else if(nilai3 < 0){
                System.out.println("Grafik memotong sumbu Y, dibawah sumbu X");
              }else{
                System.out.println("Grafik melalui titik (0,0)");
            }

            }else{
              System.out.println("Grafik melalui titik (0,0)");

              if(nilai3 > 0){
                System.out.println("Grafik memotong sumbu Y, diatas sumbu X");
              }else if(nilai3 < 0){
                System.out.println("Grafik memotong sumbu Y, dibawah sumbu X");
              }else{
                System.out.println("Grafik melalui titik (0,0)");
              }

            }

          }else{
            System.out.println("Grafik terbuka kebawah, titik balik maksimum");

            if(nilai3 > 0){
              System.out.println("Grafik memotong sumbu Y, diatas sumbu X");

              if(nilai1*nilai2 > 0){
                System.out.println("Titik balik terletak dikiri sumbu Y");
              }else if(nilai1*nilai2 < 0){
                System.out.println("Titik balik terletak dikiri sumbu Y");
              }else{
                System.out.println("Titik balik terletak di sumbu Y");
              }

            }else if(nilai3 < 0){
              System.out.println("Grafik memotong sumbu Y, dibawah sumbu X");

              if(nilai3 > 0){
                System.out.println("Grafik memotong sumbu Y, diatas sumbu X");
              }else if(nilai3 < 0){
                System.out.println("Grafik memotong sumbu Y, dibawah sumbu X");
              }else{
                System.out.println("Grafik melalui titik (0,0)");
            }

            }else{
              System.out.println("Grafik melalui titik (0,0)");

              if(nilai3 > 0){
                System.out.println("Grafik memotong sumbu Y, diatas sumbu X");
              }else if(nilai3 < 0){
                System.out.println("Grafik memotong sumbu Y, dibawah sumbu X");
              }else{
                System.out.println("Grafik melalui titik (0,0)");
              }

             }
          }
  			
        }

        fileReaderPertama.close();
        fileReaderKedua.close();
        fileReaderKetiga.close();
        
        }catch(FileNotFoundException ex) {
            System.out.println("Unable to open file");
        }
        catch(IOException ex) {
            System.out.println("Error reading file");
        }
	}
}